variable "wordpress_persistence_size" {
  type = string
}

variable "wordpress_database_host" {
  type = string
}
variable "wordpress_database_username" {
  type = string
}
variable "wordpress_database_password" {
  type = string
}
variable "wordpress_database_database" {
  type = string
}
variable "wordpress_database_port" {
  type = string
}

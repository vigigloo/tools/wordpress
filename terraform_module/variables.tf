variable "namespace" {
  type = string
}
variable "chart_repository" {
  type    = string
  default = "oci://registry-1.docker.io/bitnamicharts/"
}
variable "chart_name" {
  type = string
}
variable "chart_version" {
  type    = string
  default = "18.1.15"
}
variable "values" {
  type    = list(string)
  default = []
}
variable "image_repository" {
  type    = string
  default = "bitnami/wordpress"
}
variable "image_tag" {
  type    = string
  default = "6.4.1-debian-11-r5"
}

variable "helm_force_update" {
  type    = bool
  default = false
}
variable "helm_recreate_pods" {
  type    = bool
  default = false
}
variable "helm_cleanup_on_fail" {
  type    = bool
  default = false
}
variable "helm_max_history" {
  type    = number
  default = 0
}

variable "limits_cpu" {
  type    = string
  default = "1000m"
}
variable "limits_memory" {
  type    = string
  default = "2Gi"
}
variable "requests_cpu" {
  type    = string
  default = "1000m"
}
variable "requests_memory" {
  type    = string
  default = "2Gi"
}

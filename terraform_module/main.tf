resource "helm_release" "wordpress" {
  chart           = "wordpress"
  repository      = var.chart_repository
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = concat([
    file("${path.module}/wordpress.yaml"),
  ], var.values)

  dynamic "set" {
    for_each = var.image_repository == null ? [] : [var.image_repository]
    content {
      name  = "image.repository"
      value = var.image_repository
    }
  }
  dynamic "set" {
    for_each = var.image_tag == null ? [] : [var.image_tag]
    content {
      name  = "image.tag"
      value = var.image_tag
    }
  }

  dynamic "set" {
    for_each = var.limits_cpu == null ? [] : [var.limits_cpu]
    content {
      name  = "resources.limits.cpu"
      value = var.limits_cpu
    }
  }
  dynamic "set" {
    for_each = var.limits_memory == null ? [] : [var.limits_memory]
    content {
      name  = "resources.limits.memory"
      value = var.limits_memory
    }
  }
  dynamic "set" {
    for_each = var.requests_cpu == null ? [] : [var.requests_cpu]
    content {
      name  = "resources.requests.cpu"
      value = var.requests_cpu
    }
  }
  dynamic "set" {
    for_each = var.requests_memory == null ? [] : [var.requests_memory]
    content {
      name  = "resources.requests.memory"
      value = var.requests_memory
    }
  }

  dynamic "set" {
    for_each = var.wordpress_persistence_size == null ? [] : [var.wordpress_persistence_size]
    content {
      name  = "persistence.size"
      value = var.wordpress_persistence_size
    }
  }

  dynamic "set" {
    for_each = var.wordpress_database_host == null ? [] : [var.wordpress_database_host]
    content {
      name  = "externalDatabase.host"
      value = var.wordpress_database_host
    }
  }
  dynamic "set" {
    for_each = var.wordpress_database_username == null ? [] : [var.wordpress_database_username]
    content {
      name  = "externalDatabase.user"
      value = var.wordpress_database_username
    }
  }
  dynamic "set" {
    for_each = var.wordpress_database_password == null ? [] : [var.wordpress_database_password]
    content {
      name  = "externalDatabase.password"
      value = var.wordpress_database_password
    }
  }
  dynamic "set" {
    for_each = var.wordpress_database_database == null ? [] : [var.wordpress_database_database]
    content {
      name  = "externalDatabase.database"
      value = var.wordpress_database_database
    }
  }
  dynamic "set" {
    for_each = var.wordpress_database_port == null ? [] : [var.wordpress_database_port]
    content {
      name  = "externalDatabase.port"
      value = var.wordpress_database_port
    }
  }
}
